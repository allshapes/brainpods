Pod::Spec.new do |s|
  s.name = "TermsConsentController"
  s.summary = "Terms and conditions consent controller."
  s.version = "0.0.1"
  s.swift_version = "5.0"
  s.homepage = "https://userbrain.co"
  s.license = {:type => "MIT", :file => "LICENSE"}
  s.authors = {"Ondrej Hanak" => "oh@ondrejhanak.cz", "Jakub Heglas" => "jakub@userbrain.co"}
  s.source = {:git => "https://bitbucket.org/userbrain/termsconsentcontroller.git", :tag => s.version.to_s}
  s.platform = :ios, "13.0"
  s.source_files = "Sources/TermsConsentController/*.swift"
  s.resources = ["Sources/TermsConsentController/Resources/Consent.storyboard", "Sources/TermsConsentController/Resources/*.lproj"]
  s.frameworks = "UIKit"
end
