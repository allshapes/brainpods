Pod::Spec.new do |s|
  s.name = "AnalyticsConsentController"
  s.summary = "Analytics and tracking consent controller."
  s.version = "0.0.8"
  s.swift_version = "5.0"
  s.homepage = "https://userbrain.co"
  s.license = {:type => "MIT", :file => "LICENSE"}
  s.authors = {"Ondrej Hanak" => "oh@ondrejhanak.cz", "Jakub Heglas" => "jakub@userbrain.co"}
  s.source = {:git => "https://bitbucket.org/userbrain/analyticsconsentcontroller.git", :tag => s.version.to_s}
  s.platform = :ios, "9.0"
  s.source_files = "Sources/AnalyticsConsentController/*.{swift,h}"
  s.resources = ["Sources/AnalyticsConsentController/Resources/*.xcassets", "Sources/AnalyticsConsentController/Resources/ConsentViewController.storyboard", "Sources/AnalyticsConsentController/Resources/*.lproj"]
  s.frameworks = "UIKit"
end
